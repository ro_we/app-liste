﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Liste
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public IList<listItems> listitems { get; private set; }
        public MainPage()
        {
            InitializeComponent();

            initializeList();
        }

        public void initializeList()
        {
            listitems = new List<listItems>();

            listitems.Add(new listItems
            {
                Name = "Mein erstes Item",
                Details = "das ist ein Detail",
                Location = "Zuhause"
            });

            BindingContext = this;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            string text = ((Editor)sender).Text;
            DisplayAlert("title", text, "cancel");
        }
    }
}
