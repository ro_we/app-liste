﻿using System;
using System.Collections.Generic;
using System.Text;

 public class listItems
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

